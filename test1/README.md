## 班级：软件工程2020级4班
## 学号：202010414426
## 姓名：赵安波


# 实验1：SQL语句的执行计划分析与优化指导

## 实验目的
分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

## 实验数据库和用户

  数据库是pdborcl，用户是sys和hr

## 实验内容

- 对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
- 设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。

## 代码

- 查询1：

SELECT d.department_name, COUNT(e.job_id) AS "部门总人数",
  AVG(e.salary) AS "平均工资",
  MAX(e.salary) AS "最高薪资",
  MIN(e.salary) AS "最低薪资",
  PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY e.salary) AS "薪资中位数"
FROM hr.departments d
JOIN hr.employees e ON d.department_id = e.department_id
WHERE d.department_name IN ('Shipping', 'Purchasing')
GROUP BY d.department_name;

DEPARTMENT_NAME           部门总人数  平均工资  最高薪资   最低薪资  薪资中位数

----------------------------- ---------- ---------- ---------- ----------  ----------

Purchasing				        6	    4150	  11000      2500     2850

Shipping			         45      3475.55556	  8200     2100     3100

====================================================================================


- 分析：用于计算 "Shipping" 和 "Purchasing" 两个部门的统计信息，包括总人数、平均工资、最高薪资、最低薪资和薪资中位数。使用了合适的聚合函数和表连接，可以在单个查询中完成多个数据分析任务，提高了代码的效率。

- 查询2

WITH dept_names AS (
    SELECT department_id 
    FROM hr.departments 
    WHERE department_name IN ('Shipping', 'Purchasing')
)
SELECT d.department_name, 
       COUNT(e.job_id) AS "部门总人数",
       AVG(e.salary) AS "平均工资",
       MAX(e.salary) AS "最高薪资",
       MIN(e.salary) AS "最低薪资",
       PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY e.salary) AS "薪资中位数"
FROM hr.departments d
JOIN hr.employees e ON d.department_id = e.department_id
WHERE d.department_id IN (SELECT department_id FROM dept_names)
GROUP BY d.department_name;

输出结果：

DEPARTMENT_NAME           部门总人数  平均工资  最高薪资   最低薪资  薪资中位数

----------------------------- ---------- ---------- ---------- ----------  ----------

Purchasing				        6	    4150	  11000      2500     2850

Shipping			         45      3475.55556	  8200     2100     3100

====================================================================================

- 分析：计算 "Shipping" 和 "Purchasing" 两个部门的统计信息，包括总人数、平均工资、最高薪资、最低薪资和薪资中位数。使用了子查询和 CTE(Common Table Expression)，CTE 中定义了需要查询的部门 ID，然后在查询中使用了这些 ID 来限制结果集，同样使用了合适的聚合函数和表连接，可以在单个查询中完成多个数据分析任务，提高了代码的效率。

#### 代码比较
- 在这两个代码中，第一个代码片段更简洁和直接，没有使用 CTE 和子查询，因此查询优化器可以更容易地优化查询计划，因此第一个代码更加高效。不过，这取决于具体的数据库系统和数据量大小。
因此，如果数据量不是非常大，那么第一个代码是更简洁和直接的查询。但如果遇到需要更大的灵活性，或者数据量非常大，那么第二个代码片段可能更适合，因为使用 CTE 和子查询可以使查询更加灵活和可重用。

#### 优化建议
   考虑运行可以改进物理方案设计的访问指导或者创建推荐的索引。
- create index HR.IDX$$_00160001 on HR.DEPARTMENTS("DEPARTMENT_NAME","DEPARTM
    ENT_ID");

![%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%202023-03-16%20200140.png](attachment:%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%202023-03-16%20200140.png)


```python

```
