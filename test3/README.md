# 实验3：创建分区表
## 班级：软件工程2020级4班
## 学号：202010414426
## 姓名：赵安波
## 实验目的

掌握分区表的创建方法，掌握各种分区方式的使用场景。

## 实验内容

- 本实验使用实验2的sale用户创建两张表：订单表(orders)与订单详表(order_details)。
- 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。
- 新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。
- orders表按订单日期（order_date）设置范围分区。
- order_details表设置引用分区。
- 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。
- 写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。
- 进行分区与不分区的对比实验。

## 实验参考

- 使用sql-developer软件创建表，并导出类似以下的脚本。
- 以下脚本不含orders.customer_name的索引，不含序列设置，仅供参考。
## 实验步骤
- 创建order表的语句如下：
```sql
CREATE TABLE orders 
(
 order_id NUMBER(9, 0) NOT NULL
 , customer_name VARCHAR2(40 BYTE) NOT NULL 
 , customer_tel VARCHAR2(40 BYTE) NOT NULL 
 , order_date DATE NOT NULL 
 , employee_id NUMBER(6, 0) NOT NULL 
 , discount NUMBER(8, 2) DEFAULT 0 
 , trade_receivable NUMBER(8, 2) DEFAULT 0 
 , CONSTRAINT ORDERS_PK PRIMARY KEY 
  (
    ORDER_ID 
  )
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE (   BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL 

PARTITION BY RANGE (order_date) 
(
 PARTITION PARTITION_BEFORE_2016 VALUES LESS THAN (
 TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
 'NLS_CALENDAR=GREGORIAN')) 
 NOLOGGING
 TABLESPACE USERS
 PCTFREE 10 
 INITRANS 1 
 STORAGE 
( 
 INITIAL 8388608 
 NEXT 1048576 
 MINEXTENTS 1 
 MAXEXTENTS UNLIMITED 
 BUFFER_POOL DEFAULT 
) 
NOCOMPRESS NO INMEMORY  
, PARTITION PARTITION_BEFORE_2020 VALUES LESS THAN (
TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING
TABLESPACE USERS
, PARTITION PARTITION_BEFORE_2021 VALUES LESS THAN (
TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING 
TABLESPACE USERS
);
--以后再逐年增加新年份的分区
ALTER TABLE orders ADD PARTITION partition_before_2022
VALUES LESS THAN(TO_DATE('2022-01-01','YYYY-MM-DD'))
TABLESPACE USERS;

```
![](step1.png)
- 创建order_details表的语句如下：

```sql
CREATE TABLE order_details
(
id NUMBER(9, 0) NOT NULL 
, order_id NUMBER(10, 0) NOT NULL
, product_id VARCHAR2(40 BYTE) NOT NULL 
, product_num NUMBER(8, 2) NOT NULL 
, product_price NUMBER(8, 2) NOT NULL 
, CONSTRAINT ORDER_DETAILS_PK PRIMARY KEY 
  (
    id 
  )
, CONSTRAINT order_details_fk1 FOREIGN KEY  (order_id)
REFERENCES orders  (  order_id   )
ENABLE
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE ( BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL
PARTITION BY REFERENCE (order_details_fk1);

```
![](step2.png)

- 创建order_details表的语句如下：
CREATE SEQUENCE  SEQ1  MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
![](step2.2.png)

- 插入100条orders记录的脚本如下：

```sql
declare 
   i integer;
   y integer;
   m integer;
   d integer;
   str varchar2(100);
BEGIN  
  i:=0;
  y:=2015;
  m:=1;
  d:=12;
  while i<100 loop
    i := i+1;
    --在这里改变y,m,d
    m:=m+1;
    if m>12 then
        m:=1;
    end if;
    str:=y||'-'||m||'-'||d;
    insert into orders(order_id,order_date) 
      values(SEQ1.nextval,to_date(str,'yyyy-MM-dd'));
  end loop;
  commit;
END;
```
![](step3.png)

- 联合查询语句：
SELECT o.order_id, o.customer_name, o.order_date, d.product_id, d.product_num, d.product_price
FROM orders o
JOIN order_details d ON o.order_id = d.order_id
WHERE o.order_date BETWEEN TO_DATE('2015-01-01', 'YYYY-MM-DD') AND TO_DATE('2023-01-01', 'YYYY-MM-DD');
![](step4.png)


- 执行计划分析：
 对于orders表，使用了分区扫描，可以看到查询只扫描了3个分区（PARTITION_BEFORE_2016, PARTITION_BEFORE_2020, PARTITION_BEFORE_2021）
 对于order_details表，使用了引用分区，同样只扫描了3个分区，和orders表扫描的分区一致
 对于两张表的连接操作，使用了哈希连接，可以看到分区的优化效果比较明显
 可以通过EXPLAIN PLAN命令来查看执行计划


- 不分区实验：
 重新建表时，去掉分区相关的代码即可
 CREATE TABLE orders ...
 CREATE TABLE order_details ...
 CREATE SEQUENCE ...
 插入数据同上

 
- 对比实验：
 在使用分区和不使用分区的情况下，分别执行以上查询语句，并记录执行时间，可以发现使用分区的查询时间明显优于不使用分区的查询时间。
 这是因为使用分区可以减少扫描的数据量，提高查询效率。同时，对于插入和删除等操作，分区也可以提高性能。


## 实验注意事项

- 完成时间：2023-05-08，请按时完成实验，过时扣分。
- 查询语句及分析文档`必须提交`到：你的oracle项目中的test3目录中。
- 实验分析及结果文档说明书用Markdown格式编写。
