# 实验2：用户及权限管理
- 学号：202010414426，姓名：赵安波，班级：软工四班
## 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色TT，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有TT的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户zab，给用户分配表空间，设置限额为50M，授予TT角色。
- 最后测试：用新用户zab连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

## 实验参考步骤

对于以下的对象名称TT，zab，在实验的时候应该修改为自己的名称。

- 第1步：以system登录到pdborcl，创建角色TT和用户zab，并授权和分配空间：

```sql
$ sqlplus system/123@pdborcl
CREATE ROLE TT;
GRANT connect,resource,CREATE VIEW TO TT;
CREATE USER zab IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
ALTER USER zab default TABLESPACE "USERS";
ALTER USER zab QUOTA 50M ON users;
GRANT TT TO zab;
--收回角色
REVOKE TT FROM zab;
```

> 语句“ALTER USER zab QUOTA 50M ON users;”是指授权zab用户访问users表空间，空间限额是50M。
![](step1.png)
- 第2步：新用户zab连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。

```sql
$ sqlplus zab/123@pdborcl
SQL> show user;
USER is "zab"
SQL>
SELECT * FROM session_privs;
SELECT * FROM session_roles;
SQL>
CREATE TABLE customers (id number,name varchar(50)) TABLESPACE "USERS" ;
INSERT INTO customers(id,name)VALUES(1,'zhang');
INSERT INTO customers(id,name)VALUES (2,'wang');
CREATE VIEW customers_view AS SELECT name FROM customers;
GRANT SELECT ON customers_view TO hr;
SELECT * FROM customers_view;
NAME
--------------------------------------------------
zhang
wang
```
![](step2.1.png)
![](step2.2.png)
- 第3步：用户hr连接到pdborcl，查询zab授予它的视图customers_view

```sql
$ sqlplus hr/123@pdborcl
SQL> SELECT * FROM zab.customers;
elect * from zab.customers
                   *
第 1 行出现错误:
ORA-00942: 表或视图不存在

SQL> SELECT * FROM zab.customers_view;
NAME
--------------------------------------------------
zhang
wang
```

> 测试一下用户hr,zab之间的表的共享，只读共享和读写共享都测试一下。
> zab用户只共享了视图customers_view给hr,没有共享表customers。所以hr无法查询表customers。
![](step3.png)
![](test.png)
## 概要文件设置,用户最多登录时最多只能错误3次

```sql
$ sqlplus system/123@pdborcl
SQL>ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;
```

- 设置后，sqlplus hr/错误密码@pdborcl 。3次错误密码后，用户被锁定。
- 锁定后，通过system用户登录，alter user zab unlock命令解锁。

```sh
$ sqlplus system/123@pdborcl
SQL> alter user zab  account unlock;
```
![](step4.png)
![](step5.png)
## 数据库和表空间占用分析

> 当实验做完之后，数据库pdborcl中包含了新的角色TT和用户zab。
> 新用户zab使用默认表空间users存储表的数据。
> 随着用户往表中插入数据，表空间的磁盘使用量会增加。

## 查看数据库的使用情况

以下样例查看表空间的数据库文件，以及每个文件的磁盘占用情况。

```sql
$ sqlplus system/123@pdborcl

SQL>SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';

SQL>SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
 where  a.tablespace_name = b.tablespace_name;
```

- autoextensible是显示表空间中的数据文件是否自动增加。
- MAX_MB是指数据文件的最大容量。
![](step6.png)
## 实验结束删除用户和角色

```sh
$ sqlplus system/123@pdborcl
SQL>
drop role TT;
drop user zab cascade;
```
![](step7.png)

## 实验参考

- SQL-DEVELOPER修改用户的操作界面：
![](../img/sqldevelop修改用户.png)

- sqldeveloper授权对象的操作界面：
![](../img/sqldevelop授权对象.png)

## 结论
- 通过本次实验，创建了一个新的本地角色TT，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，任何拥有TT的用户就同时拥有这三种权限。还创建了一个新用户zab，并授予了该用户TT角色，同时限制其访问users表空间的空间限额为50M。测试了zab用户的连接和操作，包括创建表、插入数据、创建视图和授权SELECT权限给其他用户。最后还学习了如何设置概要文件，限制用户最多登录时最多只能错误3次，并学习了如何查看数据库和表空间的使用情况。
## 实验注意事项

- 完成时间：2023-04-25，请按时完成实验，过时扣分。
- 查询语句及分析文档`必须提交`到：你的oracle项目中的test2目录中。
- 实验分析及结果文档说明书用Markdown格式编写。
