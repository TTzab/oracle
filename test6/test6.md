<!-- markdownlint-disable MD033-->
<!-- 禁止MD033类型的警告 https://www.npmjs.com/package/markdownlint -->

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 | [返回](./README.md)
## 班级：软件工程2020级4班
## 学号：202010414426
## 姓名：赵安波
- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。
## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

### 总体方案

#### 表空间设计方案：

- 主表空间（sales_data）：用于存储主要的表和索引数据。
- 索引表空间（sales_index）：用于存储索引数据，提高查询性能。
- 表设计方案：
- a. 商品表（Products）：存储商品信息，包括商品ID、名称、描述、价格字段。
- b. 客户表（Customers）：存储客户信息，包括客户ID、姓名、地址、联系方式字段。
- c. 销售订单表（SalesOrders）：存储销售订单信息，包括订单ID、订单日期、客户ID字段。
- d. 销售订单详情表（SalesOrderDetails）：存储销售订单的商品明细，包括订单ID、商品ID、数量、单价字段。

#### 权限与用户分配方案：
 
- 创建两个用户：SALES_APP_USER和SALES_ADMIN_USER。
- SALE_APP_USER用户用于应用程序访问数据库，具有对商品表、客户表、销售订单表和销售订单详情表的增删改查权限。
- SALE_ADMIN_USER用户用于管理数据库，具有对表空间、用户、存储过程和备份操作的权限。

#### 存储过程与函数设计：

- 创建一个程序包（SALES_PACKAGE）来封装存储过程和函数。
- 存储过程：
- CREATE_SALES_ORDER: 用于创建新的销售订单。接收客户ID、商品ID和数量作为参数，并将订单和订单详情插入到相应的表中，完成订单的创建过程。
- CANCEL_SALES_ORDER: 用于取消已有的销售订单。接收订单ID作为参数，并删除对应的订单和订单详情记录，从数据库中取消该订单。
- CANCEL_ORDERS_BY_CUSTOMER: 用于批量取消某个客户的订单。接收客户ID作为参数，并删除该客户的所有订单和订单详情记录，实现批量取消订单的功能。
- 函数：
- CALCULATE_ORDER_TOTAL:用于根据订单ID计算订单的总金额。接收订单ID作为参数，在数据库中检索订单详情，并计算订单中商品的总金额，然后返回计算得到的总金额值。
- 以及包括将所有的查询语句封装成函数。
#### 数据库备份方案：

- 定期执行完整备份：使用Oracle的RMAN工具，定期执行完整备份，将数据库的所有数据备份。

### 具体步骤

##### 第1步：表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。

```sql
-- 创建主表空间
CREATE TABLESPACE sales_data
  DATAFILE 'sales_data.dbf'
  SIZE 100M
  AUTOEXTEND ON
  NEXT 100M
  MAXSIZE UNLIMITED;

-- 创建索引表空间
CREATE TABLESPACE sales_index
  DATAFILE 'sales_index.dbf'
  SIZE 50M
  AUTOEXTEND ON
  NEXT 50M
  MAXSIZE UNLIMITED;
  ```
 ![](step1.1.png)

```sql
-- 创建商品表
CREATE TABLE Products (
  ProductID NUMBER PRIMARY KEY,
  Name VARCHAR2(100),
  Description VARCHAR2(255),
  Price NUMBER
) TABLESPACE SALES_TS;

-- 创建客户表
CREATE TABLE Customers (
  CustomerID NUMBER PRIMARY KEY,
  Name VARCHAR2(100),
  Address VARCHAR2(255),
  Contact VARCHAR2(100)
) TABLESPACE SALES_TS;

-- 创建销售订单表
CREATE TABLE SalesOrders (
  OrderID NUMBER PRIMARY KEY,
  OrderDate DATE,
  CustomerID NUMBER,
  CONSTRAINT fk_customer
    FOREIGN KEY (CustomerID)
    REFERENCES Customers(CustomerID)
) TABLESPACE SALES_TS;

-- 创建销售订单详情表
CREATE TABLE SalesOrderDetails (
  OrderID NUMBER,
  ProductID NUMBER,
  Quantity NUMBER,
  Price NUMBER,
  CONSTRAINT pk_sales_order_details
    PRIMARY KEY (OrderID, ProductID),
  CONSTRAINT fk_order
    FOREIGN KEY (OrderID)
    REFERENCES SalesOrders(OrderID),
  CONSTRAINT fk_product
    FOREIGN KEY (ProductID)
    REFERENCES Products(ProductID)
) TABLESPACE SALES_TS;
```
![](step1.2.png)

```sql
-- 插入模拟数据
BEGIN
  FOR i IN 1..100000 LOOP
    DECLARE
      v_OrderID NUMBER;
      v_ProductID NUMBER;
      v_CustomerID NUMBER;
      v_Quantity NUMBER;
      v_Price NUMBER;
    BEGIN
      -- 插入商品表数据并获取价格
      v_ProductID := i;
      INSERT INTO Products(ProductID, Name, Description, Price)
      VALUES(v_ProductID, 'Product ' || v_ProductID, 'Description ' || v_ProductID, ROUND(DBMS_RANDOM.VALUE(1, 100), 2));

      -- 插入客户表数据
      v_CustomerID := i;
      INSERT INTO Customers(CustomerID, Name, Address, Contact)
      VALUES(v_CustomerID, 'Customer ' || v_CustomerID, 'Address ' || v_CustomerID, 'Contact ' || v_CustomerID);

      -- 插入销售订单表数据
      v_OrderID := i;
      INSERT INTO SalesOrders(OrderID, OrderDate, CustomerID)
      VALUES(v_OrderID, SYSDATE, v_CustomerID);

      -- 插入销售订单详情表数据并计算价格
      v_Quantity := ROUND(DBMS_RANDOM.VALUE(1, 10));
      SELECT Price INTO v_Price FROM Products WHERE ProductID = v_ProductID;
      v_Price := v_Price * v_Quantity;

      INSERT INTO SalesOrderDetails(OrderID, ProductID, Quantity, Price)
      VALUES(v_OrderID, v_ProductID, v_Quantity, v_Price);
    END;
  END LOOP;
  COMMIT;
END;
/
```
![](step1.3.png)

##### 第2步：设计权限及用户分配方案。至少两个用户。
```sql
-- 创建SALE_APP_USER用户并分配权限
CREATE USER SALE_APP_USER IDENTIFIED BY password DEFAULT TABLESPACE sales_data;
GRANT CREATE SESSION, CREATE TABLE, CREATE SEQUENCE, CREATE PROCEDURE TO SALE_APP_USER;
GRANT UNLIMITED TABLESPACE TO SALE_APP_USER;

-- 创建SALE_ADMIN_USER用户并分配权限
CREATE USER SALE_ADMIN_USER IDENTIFIED BY password DEFAULT TABLESPACE sales_data;
GRANT DBA TO SALE_ADMIN_USER;
创建的两个用户和他们对应的权限，下面是他们能够进行的一些操作：

用户：SALE_APP_USER
可以连接到数据库：sqlplus SALE_APP_USER/password
可以创建表：CREATE TABLE table_name (...)
可以创建序列：CREATE SEQUENCE sequence_name ...
可以创建存储过程：CREATE PROCEDURE procedure_name (...) AS ...
可以执行已存在的存储过程：EXECUTE procedure_name
可以插入、更新和删除表中的数据
可以查询表中的数据：SELECT * FROM table_name
可以创建新的会话和连接到数据库

用户：SALE_ADMIN_USER
可以连接到数据库：sqlplus SALE_ADMIN_USER/password
拥有DBA权限，具有数据库管理员权限，可以执行数据库的管理和维护操作，例如创建、修改和删除表、视图、索引等
可以执行任意的SQL语句
可以创建和管理用户和角色
可以进行备份和恢复数据库操作
可以管理数据库的安全性和权限
```
![](step2.png)

##### 第3步：在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
```sql
create or replace PACKAGE SALES_PACKAGE AS
  -- 存储过程：创建销售订单
  PROCEDURE CREATE_SALES_ORDER(
    p_customer_id IN NUMBER,
    p_product_id IN NUMBER,
    p_quantity IN NUMBER
  );

  -- 存储过程：取消销售订单
  PROCEDURE CANCEL_SALES_ORDER(
    p_order_id IN NUMBER
  );


  -- 存储过程：批量取消订单
  PROCEDURE CANCEL_ORDERS_BY_CUSTOMER(
    p_customer_id IN NUMBER
  );
  -- 函数：计算订单总金额
  FUNCTION CALCULATE_ORDER_TOTAL(
    p_order_id IN NUMBER
  ) RETURN NUMBER;

  -- 函数：查询指定商品的销售统计信息
  FUNCTION GET_PRODUCT_SALES_STATS(
    p_product_id IN NUMBER
  ) RETURN SYS_REFCURSOR;

  -- 查询所有商品
  FUNCTION GET_ALL_PRODUCTS RETURN SYS_REFCURSOR;

  -- 查询价格大于等于指定值的商品
  FUNCTION GET_PRODUCTS_BY_PRICE(
    p_min_price IN NUMBER
  ) RETURN SYS_REFCURSOR;

  -- 查询指定订单ID的订单详情
  FUNCTION GET_ORDER_DETAILS_BY_ORDER_ID(
    p_order_id IN NUMBER
  ) RETURN SYS_REFCURSOR;

  -- 查询价格大于等于指定值的订单详情
  FUNCTION GET_ORDER_DETAILS_BY_PRICE(
    p_min_price IN NUMBER
  ) RETURN SYS_REFCURSOR;

  -- 查询消费最高的用户
  FUNCTION GET_TOP_SPENDING_CUSTOMER RETURN SYS_REFCURSOR;

  -- 查询消费的中位数
  FUNCTION GET_SPENDING_MEDIAN RETURN NUMBER;

  -- 查询消费最频繁的金额和次数
  FUNCTION GET_TOP_SPENDING_FREQUENCY RETURN SYS_REFCURSOR;
END SALES_PACKAGE;
```
![](step3.1.png)


- 调用函数、存储过程和查询：
 - 查询消费最高的用户
```sql
SET SERVEROUTPUT ON;
DECLARE
  cur SYS_REFCURSOR;
  v_customer_id NUMBER;
  v_customer_name VARCHAR2(100);
  v_total_spent NUMBER;
BEGIN
  cur := SALES_PACKAGE.GET_TOP_SPENDING_CUSTOMER;

  -- 从游标中获取结果
  FETCH cur INTO v_customer_id, v_customer_name, v_total_spent;

  -- 检查是否成功获取结果
  IF cur%FOUND THEN
    DBMS_OUTPUT.PUT_LINE('最高消费用户：');
    DBMS_OUTPUT.PUT_LINE('用户ID: ' || v_customer_id);
    DBMS_OUTPUT.PUT_LINE('用户姓名: ' || v_customer_name);
    DBMS_OUTPUT.PUT_LINE('消费总额: ' || v_total_spent);
  ELSE
    DBMS_OUTPUT.PUT_LINE('没有找到消费最高的用户。');
  END IF;

  CLOSE cur;
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('错误: ' || SQLCODE || ' - ' || SQLERRM);
END;
```
![](step3.4.png)

 - 查询价格大于等于指定值的订单详情
```sql
DECLARE
  cur SYS_REFCURSOR;
  v_min_price NUMBER := &p_min_price; -- 输入最低价格
  v_product_id NUMBER;
  v_product_name VARCHAR2(100);
  v_product_price NUMBER;
BEGIN
  cur := SALES_PACKAGE.GET_PRODUCTS_BY_PRICE(v_min_price);
  LOOP
    FETCH cur INTO v_product_id, v_product_name, v_product_price;
    EXIT WHEN cur%NOTFOUND;
    DBMS_OUTPUT.PUT_LINE('商品ID: ' || v_product_id || ', 商品名称: ' || v_product_name || ', 商品价格: ' || v_product_price);
  END LOOP;
  CLOSE cur;
  DBMS_OUTPUT.PUT_LINE('查询完成。');
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('错误: ' || SQLCODE || ' - ' || SQLERRM);
END;
```
![](step3.2.png)

 - 查询所有商品
```sql
DECLARE
  cur SYS_REFCURSOR;
BEGIN
  cur := SALES_PACKAGE.GET_ALL_PRODUCTS;
  -- 使用游标进行操作，例如循环遍历结果集
  -- ...
  DBMS_OUTPUT.PUT_LINE('查询完成。');
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('错误: ' || SQLCODE || ' - ' || SQLERRM);
END;
```
![](step3.3.png)

 - 根据用户id批量取消订单
```sql
SET SERVEROUTPUT ON;
DECLARE
  v_order_id NUMBER;
BEGIN
  -- 从用户获取订单ID
  v_order_id := &p_order_id; -- 输入订单ID

  SALES_PACKAGE.CANCEL_SALES_ORDER(v_order_id);
  DBMS_OUTPUT.PUT_LINE('订单已取消。');
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('错误: ' || SQLCODE || ' - ' || SQLERRM);
END;
```
![](step3.5.png)
![](step3.6.png)

 - 查询消费最频繁的金额和次数
```sql
SET SERVEROUTPUT ON;
DECLARE
  cur SYS_REFCURSOR;
  v_total_spent NUMBER;
  v_frequency NUMBER;
BEGIN
  cur := SALES_PACKAGE.GET_TOP_SPENDING_FREQUENCY;
  LOOP
    FETCH cur INTO v_total_spent, v_frequency;
    EXIT WHEN cur%NOTFOUND;
    DBMS_OUTPUT.PUT_LINE('消费金额: ' || v_total_spent || ', 次数: ' || v_frequency);
  END LOOP;
  CLOSE cur;
  DBMS_OUTPUT.PUT_LINE('查询完成。');
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('错误: ' || SQLCODE || ' - ' || SQLERRM);
END;
```
![](step3.7.png)

 - 查询指定商品的销售统计信息
```sql
DECLARE
  v_result NUMBER;
BEGIN
  v_result := SALES_PACKAGE.CALCULATE_ORDER_TOTAL(712);
  -- 处理返回的结果
  DBMS_OUTPUT.PUT_LINE('Order Total: ' || v_result);
END;
```
![](step3.8.png)
  
##### 第4步：设计一套数据库的备份方案。

- 备份方案：商品销售系统的数据库备份
- 这个备份方案旨在提供一种保护商品数据库数据的方法。通过执行数据库备份，创建数据库的完整副本，以便在发生数据损坏、系统故障或意外数据丢失时进行恢复

- 使用sqlplus命令以sysdba用户身份登录到Oracle数据库，该命令如下：
```sql
$ sqlplus / as sysdba
```
- 关闭数据库并以MOUNT模式重新启动数据库，该命令如下：
```sql
SQL> SHUTDOWN IMMEDIATE
SQL> STARTUP MOUNT
```
- 将数据库切换到归档模式，该命令如下：
```sql
SQL> ALTER DATABASE ARCHIVELOG;
```
- 将数据库打开，该命令如下：
```sql
SQL> ALTER DATABASE OPEN;
```
![](step4.1.png)

- 使用rman命令以目标用户身份连接到Oracle数据库，该命令如下：
```sql
$ rman target /
```
- 显示rman的配置信息，该命令如下：
```sql
RMAN> SHOW ALL;
```
![](step4.2.png)

- 对整个数据库执行备份，该命令如下：
```sql
RMAN> BACKUP DATABASE;
```
![](step4.3.png)

- 列出备份信息，该命令如下：
```sql
RMAN> LIST BACKUP;
```
![](step4.4.png)

- 备份方案将执行完整的数据库备份，并列出备份信息。可以根据需要定期执行此备份方案来保护商品数据库的数据。